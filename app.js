var x = [5,13,4,7,1,9,6,6];
console.log(x);
console.log(smallToLarge(x));
console.log(largeToSmall(x));

function smallToLarge(a) {
    let x = a.length;
    let temp;
    for(var i = 0; i < x; i++){
        for(var j = 0; j < x - 1; j++){
            if (a[j] > a[j+1]) {
                temp = a[j];
                a[j] = a[j+1];
                a[j+1] = temp;
            }
        }
    }
    return a;
}


function largeToSmall(a) {
    for (let i = 0; i < a.length; i++) {
        for (let j = 0; j < a.length - 1; j++) {
            if (a[j] < a[j+1]) {
                let temp = a[j];
                a[j] = a[j+1];
                a[j+1] = temp;
            }
        }
    }
    return a;
}

